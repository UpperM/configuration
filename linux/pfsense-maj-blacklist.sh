#!/bin/sh
##### SCRIPT DE MISE A JOUR DES BLACKLISTS POUR PFSENSE 2.4+ #####
# r2: ajout detection des erreurs sur DL et reduction taille globale

# Deplacement vers /tmp
cd /tmp/

# Suppression de l'ancienne DB
rm blacklists_cmp.tar.gz

# Telechargement des 2 DB Shalla et Capitole
erreur=1
compteur=0
while [ $erreur -ne 0 ]
do
  fetch http://dsi.ut-capitole.fr/blacklists/download/blacklists_for_pfsense.tar.gz
  erreur=$?
  sleep 10
  compteur=`expr $compteur + 1`
  if [ $compteur -gt 30 ]
  then
    echo "ERREUR: Impossible de telecharger la DB CAPITOLE"
    exit $erreur
  fi
done

erreur=1
while [ $erreur -ne 0 ]
do
  fetch http://www.shallalist.de/Downloads/shallalist.tar.gz
  erreur=$?
  sleep 10
  compteur=`expr $compteur + 1`
  if [ $compteur -gt 30 ]
  then
    echo "ERREUR: Impossible de telecharger la DB SHALLA"
    exit $erreur
  fi
done

# Decompression et suppression des tar.gz
tar -xzf blacklists_for_pfsense.tar.gz
erreur=$?
if [ $erreur -ne 0 ]
then
  echo "ERREUR: Impossible de decompresser la DB CAPITOLE"
  exit $erreur
fi
tar -xzf shallalist.tar.gz
erreur=$?
if [ $erreur -ne 0 ]
then
  echo "ERREUR: Impossible de decompresser la DB SHALLA"
  exit $erreur
fi
rm blacklists_for_pfsense.tar.gz
rm shallalist.tar.gz

# Creation des dossiers et sous-dossiers blacklists_cmp/
mkdir -p blacklists_cmp/{BL_Dangeureux_Sexe_Hack_Alcool,ContournementProxy,Jeux_PariEnLigne_Finances,Religions_Sectes,Chat,Blog_Celebrites_Astro,Cuisine_Sonneries_Rencontres_Sports,Publicite_Bannieres,Hopitaux,Desinfections,MiseAJour,Mail_MoteurRecherche,Education_Emploi_Gouvernement,Hebergeurs_Videos_Radio_Images_Cloud,Forums,PriseEnMainADistance,Informations_Presses,AchatEnLigne_Amazon_Ikea,Banques_Assurances,DynDNS}

# Fusion des fichiers domains et urls puis suppression des temporaires
# Localisation Capitole
cat blacklists/{adult,agressif,bitcoin,cryptojacking,dangerous_material,ddos,drogue,hacking,lingerie,malware,manga,marketingware,mixed_adult,phishing,reaffected,sexual_education,tricheur,warez}/domains >> domains
cat blacklists/{adult,agressif,bitcoin,cryptojacking,dangerous_material,drogue,hacking,lingerie,malware,manga,marketingware,mixed_adult,phishing,reaffected,sexual_education,tricheur,warez}/urls >> urls
#cat blacklists/ddos/urls >> urls

# Localisation Shalla
cat BL/{aggressive,alcohol,costtraps,drugs,hacking,porn,sex/education,sex/lingerie,spyware,tracker,violence,warez,weapons}/domains >> domains
cat BL/{aggressive,alcohol,costtraps,drugs,hacking,porn,sex/education,sex/lingerie,spyware,tracker,violence,warez,weapons}/urls >> urls

sort -g domains | uniq > blacklists_cmp/BL_Dangeureux_Sexe_Hack_Alcool/domains
sort -g urls | uniq > blacklists_cmp/BL_Dangeureux_Sexe_Hack_Alcool/urls
rm domains urls

# Localisation Capitole
cat blacklists/{redirector,shortener,strict_redirector,strong_redirector}/domains >> domains
cat blacklists/{redirector,shortener,strict_redirector,strong_redirector}/urls >> urls

# Localisation Shalla
cat BL/{anonvpn,redirector,urlshortener}/domains >> domains
cat BL/{anonvpn,redirector,urlshortener}/urls >> urls

sort -g domains | uniq > blacklists_cmp/ContournementProxy/domains
sort -g urls | uniq > blacklists_cmp/ContournementProxy/urls
rm domains urls

# Localisation Capitole
cat blacklists/{arjel,financial,gambling,games,special}/domains >> domains
cat blacklists/{financial,gambling,games,special}/urls >> urls
#cat blacklists/arjel/urls >> urls

# Localisation Shalla
cat BL/{gamble,finance/other,finance/trading}/domains >> domains
cat BL/{gamble,finance/other,finance/trading}/urls >> urls

sort -g domains | uniq > blacklists_cmp/Jeux_PariEnLigne_Finances/domains
sort -g urls | uniq > blacklists_cmp/Jeux_PariEnLigne_Finances/urls
rm domains urls

# Localisation Capitole
cat blacklists/{associations_religieuses,sect}/domains >> domains
cat blacklists/sect/urls >> urls
#cat blacklists/associations_religieuses/urls >> urls

# Localisation Shalla
cat BL/religion/domains >> domains
cat BL/religion/urls >> urls

sort -g domains | uniq > blacklists_cmp/Religions_Sectes/domains
sort -g urls | uniq > blacklists_cmp/Religions_Sectes/urls
rm domains urls

# Localisation Capitole
cat blacklists/chat/domains >> domains
cat blacklists/chat/urls >> urls

# Localisation Shalla
cat BL/chat/domains >> domains
cat BL/chat/urls >> urls

sort -g domains | uniq > blacklists_cmp/Chat/domains
sort -g urls | uniq > blacklists_cmp/Chat/urls
rm domains urls

# Localisation Capitole
cat blacklists/{astrology,blog,celebrity}/domains >> domains
cat blacklists/{astrology,blog,celebrity}/urls >> urls

# Localisation Shalla
cat BL/{fortunetelling,models}/domains >> domains
cat BL/{fortunetelling,models}/urls >> urls

sort -g domains | uniq > blacklists_cmp/Blog_Celebrites_Astro/domains
sort -g urls | uniq > blacklists_cmp/Blog_Celebrites_Astro/urls
rm domains urls

# Localisation Capitole
cat blacklists/{cooking,dating,mobile-phone,sports}/domains >> domains
cat blacklists/{dating,mobile-phone}/urls >> urls
#cat blacklists/cooking/urls >> urls
#cat blacklists/sports/urls >> urls

# Localisation Shalla
cat BL/{automobile/{bikes,boats,cars,planes},dating,ringtones,hobby/{cooking,games-misc,games-online},gardening,pets,recreation/{humor,martialarts,restaurants,sports,travel,wellness}}/domains >> domains
cat BL/{automobile/{bikes,boats,cars,planes},dating,ringtones,hobby/{cooking,games-misc,games-online},gardening,pets,recreation/{humor,martialarts,restaurants,sports,travel,wellness}}/urls >> urls

sort -g domains | uniq > blacklists_cmp/Cuisine_Sonneries_Rencontres_Sports/domains
sort -g urls | uniq > blacklists_cmp/Cuisine_Sonneries_Rencontres_Sports/urls
rm domains urls

# Localisation Capitole
cat blacklists/publicite/domains >> domains
cat blacklists/publicite/urls >> urls

# Localisation Shalla
cat BL/adv/domains >> domains
cat BL/adv/urls >> urls

sort -g domains | uniq > blacklists_cmp/Publicite_Bannieres/domains
sort -g urls | uniq > blacklists_cmp/Publicite_Bannieres/urls
rm domains urls

# Localisation Capitole
#cat blacklists/social_network/domains >> domains
#cat blacklists/social_network/urls >> urls

# Localisation Shalla
cat BL/socialnet/domains >> domains
cat BL/socialnet/urls >> urls

sort -g domains | uniq > blacklists_cmp/ReseauxSociaux/domains
sort -g urls | uniq > blacklists_cmp/ReseauxSociaux/urls
rm domains urls

# Localisation Shalla
cat BL/hospitals/domains >> domains
cat BL/hospitals/urls >> urls

sort -g domains | uniq > blacklists_cmp/Hopitaux/domains
sort -g urls | uniq > blacklists_cmp/Hopitaux/urls
rm domains urls

# Localisation Shalla
cat blacklists/cleaning/domains >> domains
cat blacklists/cleaning/urls >> urls

sort -g domains | uniq > blacklists_cmp/Desinfections/domains
sort -g urls | uniq > blacklists_cmp/Desinfections/urls
rm domains urls

# Localisation Capitole
cat blacklists/update/domains >> domains
#cat blacklists/update/urls >> urls

# Localisation Shalla
cat BL/updatesites/domains >> domains
cat BL/updatesites/urls >> urls

sort -g domains | uniq > blacklists_cmp/MiseAJour/domains
sort -g urls | uniq > blacklists_cmp/MiseAJour/urls
rm domains urls

# Localisation Capitole
cat blacklists/{translation,webmail}/domains >> domains
cat blacklists/{translation,webmail}/urls >> urls

# Localisation Shalla
cat BL/{searchengines,webmail}/domains >> domains
cat BL/{searchengines,webmail}/urls >> urls

sort -g domains | uniq > blacklists_cmp/Mail_MoteurRecherche/domains
sort -g urls | uniq > blacklists_cmp/Mail_MoteurRecherche/urls
rm domains urls

# Localisation Capitole
cat blacklists/{child,educational_games,jobsearch,liste_blanche,liste_bu}/domains >> domains
cat blacklists/{child,educational_games,liste_blanche,liste_bu}/urls >> urls
#cat blacklists/jobsearch/urls >> urls

# Localisation Shalla
cat BL/{education/schools,government,jobsearch,military,politics,science/{astronomy,chemistry}}/domains >> domains
cat BL/{education/schools,government,jobsearch,military,politics,science/{astronomy,chemistry}}/urls >> urls

sort -g domains | uniq > blacklists_cmp/Education_Emploi_Gouvernement/domains
sort -g urls | uniq > blacklists_cmp/Education_Emploi_Gouvernement/urls
rm domains urls

# Localisation Capitole
cat blacklists/{audio-video,download,filehosting,radio}/domains >> domains
cat blacklists/{audio-video,download,filehosting,radio}/urls >> urls

# Localisation Shalla
cat BL/{downloads,imagehosting,isp,library,movies,music,podcasts,radiotv,webphone,webradio,webtv}/domains >> domains
cat BL/{downloads,imagehosting,isp,library,movies,music,podcasts,radiotv,webphone,webradio,webtv}/urls >> urls

sort -g domains | uniq > blacklists_cmp/Hebergeurs_Videos_Radio_Images_Cloud/domains
sort -g urls | uniq > blacklists_cmp/Hebergeurs_Videos_Radio_Images_Cloud/urls
rm domains urls

# Localisation Capitole
cat blacklists/forums/domains >> domains
cat blacklists/forums/urls >> urls

# Localisation Shalla
cat BL/forum/domains >> domains
cat BL/forum/urls >> urls

sort -g domains | uniq > blacklists_cmp/Forums/domains
sort -g urls | uniq > blacklists_cmp/Forums/urls
rm domains urls

# Localisation Capitole
cat blacklists/remote-control/domains >> domains
cat blacklists/remote-control/urls >> urls

# Localisation Shalla
cat BL/remotecontrol/domains >> domains
cat BL/remotecontrol/urls >> urls

sort -g domains | uniq > blacklists_cmp/PriseEnMainADistance/domains
sort -g urls | uniq > blacklists_cmp/PriseEnMainADistance/urls
rm domains urls

# Localisation Capitole
cat blacklists/press/domains >> domains
cat blacklists/press/urls >> urls

# Localisation Shalla
cat BL/news/domains >> domains
cat BL/news/urls >> urls

sort -g domains | uniq > blacklists_cmp/Informations_Presses/domains
sort -g urls | uniq > blacklists_cmp/Informations_Presses/urls
rm domains urls

# Localisation Capitole
cat blacklists/shopping/domains >> domains
cat blacklists/shopping/urls >> urls

# Localisation Shalla
cat BL/{homestyle,shopping}/domains >> domains
cat BL/{homestyle,shopping}/urls >> urls

sort -g domains | uniq > blacklists_cmp/AchatEnLigne_Amazon_Ikea/domains
sort -g urls | uniq > blacklists_cmp/AchatEnLigne_Amazon_Ikea/urls
rm domains urls

# Localisation Capitole
cat blacklists/bank/domains >> domains
#cat blacklists/bank/urls >> urls

# Localisation Shalla
cat BL/finance/{banking,insurance,moneylending,realestate}/domains >> domains
cat BL/finance/{banking,insurance,moneylending,realestate}/urls >> urls

sort -g domains | uniq > blacklists_cmp/Banques_Assurances/domains
sort -g urls | uniq > blacklists_cmp/Banques_Assurances/urls
rm domains urls

# Localisation Shalla
cat BL/dynamic/domains >> domains
cat BL/dynamic/urls >> urls

sort -g domains | uniq > blacklists_cmp/DynDNS/domains
sort -g urls | uniq > blacklists_cmp/DynDNS/urls
rm domains urls

# Compactage de la DB

tar czf blacklists_cmp.tar.gz blacklists_cmp

# Suppression des dossiers shalla, capitole et cmp

rm -rf BL/ blacklists/ blacklists_cmp/

/usr/local/bin/squidGuard_blacklist_update.sh
/usr/bin/nice -n20 /etc/rc.update_urltables
